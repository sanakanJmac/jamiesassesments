# Imported libaries required for the program
import kivy
import socket
import sys
import webbrowser

kivy.require('2.0.0')

# Below are modules that the required libaries imported
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import StringProperty
from kivy.graphics import *

# Below is the GUI being created, the size of the GUI is defined.
class FloatLayout(FloatLayout):
    Window.size = (360, 200)
    status_text = StringProperty()
    ip_address = StringProperty()

    # the code bellow finds the java server and connects to it
    def connect_to_server(self, instance, value):
        self.status_text = 'Not Connected'

        # https://realpython.com/python-sockets/#echo-client
        # The code defines where the host IP address will be and what ports are going to be used
        HOST = self.ip.text # The java servers IP address
        PORT = 6789  # Port in use on the server

        # this creates a socket and, then attempts to create a connection with the server
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(0.1)
            s.connect((HOST, PORT))
            self.status_text = 'Connection Test Successful \nDrag and Drop Files to Send'

            # This gives our client GUI the ability to take drag and drop files into the client and send them to the server
            Window.bind(on_dropfile=self._on_file_drop)

            # the code here is sending and receiving data, if the program finds a empty input value, itll display to the user on the client
            # that there was no succesful sending of the file. However if the client detects correct values or a full input value it'll respond with correctly sent
            if value != '':
                sent = str(s.send(value.encode()))
                if sent == 0:
                    self.status_text = "Failure: 0 Characters Sent"
                else:
                    print(sent + 'Characters Sent Successfully')
                    print(value)
                    self.status_text = (sent + ' Characters Sent Successfully')
                print(s)

            #This terminates connection to server
            s.shutdown(socket.SHUT_RDWR)
            s.close()
            print(s)

        # Exception handling
        except socket.timeout as e:
            self.status_text = ('Connection Timeout Failure: \n' + str(e))
            print(e)
        except socket.error as e:
            self.status_text = ('Connection Not Found: \n' + str(e))
            print(e)
        return

    # This code allows files to be added to the server, when a file is dragged and dropped on the client window, the client will read the file
    # and proceed to send the data to  connect_to_server, the connect_to_server function then sends the file on to the server
    def _on_file_drop(self, window, file_path):
        print(file_path)
        data = open(file_path, 'r').read()  # Read the file as string
        self.connect_to_server('', data)
        return

    # this creates a help button
    def help(self):
        webbrowser.open('http://192.168.56.1/index.html', new=2, autoraise=True)

    # this is the exit button or fucntion for the client
    def exit(self):
        sys.exit()


    def __init__(self, **kwargs):
        super(FloatLayout, self).__init__(**kwargs)

        # adding 2D graphics to GUI
        with self.canvas:
            # Draw a red rectangle:
            Color(0, 0, 1, 0.5, mode="rgba")
            self.rect = Rectangle(pos=(310, 0), size=(50, 50))
            # Draw a white star:
            Color(255, 255, 255, 1, mode="rgba")
            Line(points=(320, 5, 335, 45, 350, 5, 315, 30, 355, 30, 320, 5))


class MyApp(App):
    def build(self):
        self.title = 'Python client'
        return FloatLayout()


if __name__ == '__main__':
    MyApp().run()