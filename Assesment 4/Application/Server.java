package Application;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

// creates a server class
public class Server {

    public static void main (String args[]) throws IOException {

        // this will create a socket, the socket is then assigned a port number
        // so it can be listened on
        ServerSocket serverSocket = new ServerSocket(6789);
        System.out.println("Listening on Port 6789\n");

        // This will create a start menu the file MenuThread.java
        new MenuThread().start();

        // this function allows the server to accept connections on port 6789
        while(true) {
            Socket socket = serverSocket.accept();

            // new thread for client
            new EchoThread(socket).start();

            // This finds the client or user that is trying to connect to the server using their IP address
            InetAddress ip = socket.getInetAddress();
            int port = socket.getPort();

            // this prints the console to user, this displays IP and port
            System.out.print("New client connected from: " + ip + ":" + port + "\n");
        }
    }
}