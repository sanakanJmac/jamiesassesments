package Application;

import java.util.ArrayList;

// Invoice.java takes all information acquired in the reports, it then converts this information into a products list.
// the products list will then be used to make a total for each report.
public class Invoice {

    //Attributes
    private ArrayList<Product> products;
    private double total;

    //Constructor
    public Invoice(ArrayList<Product> products, double total) {
        this.products = products;
        this.total = total;
    }

    //Methods - Getters and Setters
    public ArrayList<Product> getProducts() {
        return products;
    }

    //Methods - Getters and setters
    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    //Methods - Getters and Setters
    public double getTotal() {
        return total;
    }

    //Methods - Getters and Setters
    public void setTotal(double total) {
        this.total = total;
    }

    //Method
    public String toString() {

        for(Product p: products) {
            System.out.println(p.toString());
        }
        return "Invoice Total: $" + String.format("%.2f", getTotal()) + "\n";
    }

    static class Product {

        //Attributes
        private String name;
        private double price;

        //Constructor
        public Product(String name, double price) {
            this.name = name;
            this.price = price;
        }

        //Methods - Getters and Setters
        public String getName() {
            return name;
        }

        //Methods
        public void setName(String name) {
            this.name = name;
        }

        //Methods - Getters and Setters
        public double getPrice() {
            return price;
        }

        // Methods - Getters and Setters
        public void setPrice(double price) {
            this.price = price;
        }

        //Method
        public String toString() {
            return getName() + ": " + "$" + String.format("%.2f", getPrice());
        }
    }
}