package Application;

import Application.Invoice.Product;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


// This function below creates a menu for the server and makes it show in the command prompt
public class MenuThread extends Thread{

    public void run() {

        // The code below will print a list of commands, the printed list of commands will be known to the user
        // and are expected from the users
        while(true) {
            System.out.println("Please select from the following:");
            System.out.println("Enter 'R' to generate report, the total of all invoices saved");
            System.out.println("Enter 'D' to delete all invoices from the server");
            System.out.println("Enter 'E' to terminate the server");

            Scanner scanner = new Scanner(System.in);
            System.out.println("\nEnter the task to perform");
            String task = scanner.nextLine().toUpperCase();

            // the server will wait for the user to make a choice, once the user enters their choice
            // ie print or delete the function is then performed.
            switch (task) {
                case "R":
                    System.out.println(report());
                    break;
                case "D":
                    delete();
                    break;
                case "E":
                    System.exit(0);
                default:
                    System.out.println("Invalid Task\n");
            }
        }
    }

    // This code is the report function, when a user enters R or selects R to print a report
    // this function will then run, which compiles and prints the report
    private String report() {

        // This code makes a reference to the invoice.java file, the function will
        // then start compiling a list of products
        ArrayList<Invoice.Product> products = new ArrayList<Invoice.Product>();
        double total = 0;
        Invoice invoice = new Invoice(products, total);
        List<Invoice> invoices = new ArrayList<Invoice>();

        String name = null;
        double price = 0;

        Invoice.Product product = new Invoice.Product(name, price);

        System.out.print("INVOICE REPORT:\n");

        //This code reads in the list of files in a directory which was sent from the client
        String path = System.getProperty("user.home") + File.separator + "Invoices";


        File directory = new File(path);
        File fileList[] = directory.listFiles();

        // This code looks at all files that have been added in the directory and then proceeds to read them, it reads them
        // based on what file format ie .json or .XML
        for (File file:fileList) {
            String readThisFile = file.getName().toLowerCase();

            // when the file format is .json the function will read the file, and convert the file to string.
            if(readThisFile.endsWith(".json")) {

                try {
                    //Create a Gson instance
                    Gson gson = new Gson();

                    //Create a reader
                    URI uri = file.toURI();
                    Reader reader = Files.newBufferedReader(Paths.get(uri));

                    //Convert JSON string to object
                    JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);

                    reader.close();

                    products = new ArrayList<Product>();

                    JsonArray jsonArray = jsonObject.get("invoice").getAsJsonObject().get("products").getAsJsonObject().get("product").getAsJsonArray();

                    for(JsonElement element: jsonArray) {
                        JsonObject elementAsJsonObject = element.getAsJsonObject();
                        name = elementAsJsonObject.get("name").getAsString();
                        price = elementAsJsonObject.get("price").getAsDouble();

                        product = new Product(name, price);
                        products.add(product);
                    }

                    total = jsonObject.get("invoice").getAsJsonObject().get("total").getAsDouble();

                    invoice = new Invoice(products, total);
                    invoices.add(invoice);
                    System.out.println(invoice.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }



                // this function is used for.xml files, it looks through a .xml file and looks for 'name' and 'price'
                // it then inserts them to a product list.
            }  else if (readThisFile.endsWith(".xml")) {

                try {
                    //Make an XML document
                    Document xml = null;

                    xml = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);

                    // looks through the xml for name and price
                    NodeList nameList = xml.getElementsByTagName("name");
                    NodeList priceList = xml.getElementsByTagName("price");
                    products = new ArrayList<Product>();
                    for(int i = 0; i < nameList.getLength(); i++) {
                        name = nameList.item(i).getTextContent();
                        price = Double.parseDouble(priceList.item(i).getTextContent());

                        product = new Product(name, price);
                        products.add(product);
                    }

                    NodeList nodeList = xml.getElementsByTagName("total");
                    // Function sees only one item in the list, therefore it puts it at a
                    // position of zero
                    total = Double.parseDouble(nodeList.item(0).getTextContent());

                    invoice = new Invoice(products, total);
                    invoices.add(invoice);
                    System.out.println(invoice.toString());

                    // The function bellow is for exception handling, it provides the program intgerity,
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                }


            }

        }
        // Adds the invoice totals into one sum
        total = 0;
        for(Invoice i: invoices) {
            total += i.getTotal();
        }
        // prints the output of every invoice put together
        return "The total amount for " + invoices.size() + " invoices is: $" + String.format("%.2f", total) + "\n---------------------";
    }

    // deletes all invoices being used
    private void delete() {

        // will search for an invoice and print its location
        String path = System.getProperty("user.home") + File.separator + "Invoices";

        // displays a message warning its user
        System.out.println("WARNING!!! You've chosen to DELETE ALL INVOICES!\n" +
                "This will delete all files ending with .xml or .json from " + path);

        System.out.println("The following files will be deleted: \n");

        // this function creates a list of files which are going to be deleted, so a back-up in a way
        File directory = new File(path);
        File fileList[] = directory.listFiles();

        // this function will the print the list of files which are going to be deleted
        for (File file:fileList) {
            String fileToDelete = file.getName().toLowerCase();
            if (fileToDelete.endsWith(".xml")||fileToDelete.endsWith(".json")); {
                System.out.println(fileToDelete);
            }
        }

        // displays a message to user, making the user confirm their choice by entering DELETE
        System.out.println("Type 'DELETE' and press enter to proceed \n" +
                "OR enter any key to cancel");

        // this function deletes all the files which are in the Invoices directory, it deletes files in the format of .json or .xml
        Scanner scanner = new Scanner(System.in);
        String delete = scanner.nextLine().toUpperCase();
        // when the user enetes DELETE, this function will delete all files
        if(delete.equals("DELETE")) {
            for (File file:fileList) {
                String fileToDelete = file.getName().toLowerCase();
                if (fileToDelete.endsWith(".xml")||fileToDelete.endsWith(".json")) {
                    file.delete();
                }
            }
            System.out.println("All invoices have been deleted\n");
            // This function returns the user to the main menu if they do not correctly input
            // DELETE.
        } else {
            System.out.println("Operation cancelled. No invoices were deleted\n");
        }
    }
}