package Application;

import java.io.*;
import java.net.Socket;
import java.nio.Buffer;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

// Echo thread sends the user data from the server from the client and sends it to a
// directory and ensures its stored in the appropriate format.
public class EchoThread extends Thread {

    protected Socket socket;

    public EchoThread(Socket clientSocket) {
        this.socket = clientSocket;
    }

    public void run() {

        // This function reads in user inputted files that where sent to the server
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream(), "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder out = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                out.append(line + "\n");
            }

            String clientString = out.toString();
            bufferedReader.close();

            //This function saves the file in the correct directory
            String path = System.getProperty("user.home") + File.separator + "Invoices";

            //This makes sure the correct directory exists, if the program finds it does not
            //it'll create a new directory and save the it there.
            File directory = new File(path);
            if(!directory.exists()) {
                directory.mkdir();
            }


            if(clientString.length() > 0) {
                ZonedDateTime zdt = ZonedDateTime.now();
                String time = zdt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                time = time.replaceAll(":", "-");

                String saveAs = null;

                if(clientString.startsWith("{")) {
                    saveAs = ".json";
                } else if (clientString.startsWith("<")) {
                    saveAs = ".xml";
                } else {
                    saveAs = ".txt";
                }

                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path + File.separator + time + saveAs, true));

                // This creates an array that contains every character of the invoice and puts it into a char array
                char[] charArray = clientString.toCharArray();
                // For each char in the array, append it to the file
                for(char c: charArray) {
                    bufferedWriter.append(c);
                    bufferedWriter.flush();
                }

                bufferedWriter.close();


                // if the server works as intended, it'll print the file name of the updated file
                System.out.println("File saved as: " + saveAs);

            }

            //This handles all exceptions
        } catch (IOException e) {
            System.out.print(e);
            return;
        }
    }

}