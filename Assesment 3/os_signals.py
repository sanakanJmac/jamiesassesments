#imports the libaries needed for the program
import signal
import os
import time
import sys

#function which is named read confgiruation, this will print reading configuration
def readConfiguration(singalNumber, frame):
    print('(SIGHUP) reading configuration')
    return

#function terminate, it will terminate the printing process, when the program acitvates it
#itll then exit the program
def terminateProcess(signalNumber, frame):
    print('(SIGTERM) terminating the process')
    sys.exit()

#prints signals name when recvied.
def receiveSignal(signalNumber, frame):
    print('Received:', signalNumber)
    return

if __name__ == '__main__':
# Register the signals to be caught
#labels all signals which will be used. also states the functions that will be required.

    signal.signal(signal.SIGHUP, readConfiguration)
    signal.signal(signal.SIGINT, receiveSignal)
    signal.signal(signal.SIGQUIT, receiveSignal)
    signal.signal(signal.SIGILL, receiveSignal)
    signal.signal(signal.SIGTRAP, receiveSignal)
    signal.signal(signal.SIGABRT, receiveSignal)
    signal.signal(signal.SIGBUS, receiveSignal)
    signal.signal(signal.SIGFPE, receiveSignal)
# signal.signal(signal,Sigkill, receivesignal)
    signal.signal(signal.SIGUSR1, receiveSignal)
    signal.signal(signal.SIGSEGV, receiveSignal)
    signal.signal(signal.SIGUSR2, receiveSignal)
    signal.signal(signal.SIGPIPE, receiveSignal)
    signal.signal(signal.SIGALRM, receiveSignal)
    signal.signal(signal.SIGTERM, terminateProcess)


# out put current process id, ie print
    print('My PID Is:', os.getpid())


# wait in an endless loop for signals, this means the program is running in a constant state of loop waiting for signals
    while True:
        print('Waiting...')
        time.sleep(3)
