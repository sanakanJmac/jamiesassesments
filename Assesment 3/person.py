#This Creates a class named person
class Person():

    #Consutructor, this code creates a function that defines firstname, lastname, street, city & state.
    def __init__(self, firstname, lastname, street, city, state, postcode):
        self.firstname = firstname
        self.lastname = lastname
        self.street = street
        self.city = city
        self.state = state
        self.postcode = postcode

        #Method
        def getFirstName(self):
            return self.firstname

        def getLastName(self):
            return self.lastname

        def getStreet(self):
            return self.street

        def getCity(self):
            return self.city

        def getState(self):
            return self.state

        def getPostcode(self):
            return self.postcode


    #writes a person to a csv file
    def __str__(self):
        return '{}, {}, {}, {}, {}, {}'.format(self.firstname, self.lastname, self.street, self.city, self.state, self.postcode)


