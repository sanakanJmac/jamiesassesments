#This imports the class or person.py file
from person import Person
import time

fileref = open("smallData.dat","r") # This reads the smallData.dat file
#fileref = open("mediumData.dat","r") # This reads the mediumData.dat file
#fileref = open("largeData.dat","r") # This reads the largedata.dat file


lines = fileref.readlines()

#Defines the variables makin them equal to nothing
firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

#The code below creates a loop to loop function, the loop is continous
for line in lines:
    if ',' in line:
        lastname, firstname = line.strip().split(', ')
        continue
    elif line[0].isdigit():
        street = line.strip()
        continue
    else:
        my_list = (line.strip().split('\t'))
        if len(my_list) > 1:
            city, state, postcode = my_list
            continue

    #this defines the order in which it occurs
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

def bubbleSort(people):
    n = len(people)

    #Travese through all array elements
    for i in range(n):

        #This tells the program Last i elements are already in place and don't need moving
        for j in range(0, n-i-1):

            # Traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if people[j].lastname > people[j+1].lastname:
                people[j], people[j+1] = people[j+1], people[j]


#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
#start = time.perf_counter()
#bubbleSort(people)
#end = time.perf_counter()
#bubbleSortTime = end - start
#print("Bubblesort time below")
#print(bubbleSortTime)

def mergeSort(people):
    if len(people) > 1:

        #This finds the mid point of the array
        mid = len(people)//2 # Finding the mid of the array

        #this divides the whole array into half once middle point has been found
        L = people[:mid] # Dividing the array elements
        R = people[mid:] # Into 2 halves

        mergeSort(L) # sorting the first half
        mergeSort(R) # sorting the second half

        #makes the length of string equal 0
        i = j = k = 0

        #Copys data to temporary arrays l[] and r[]
        while i < len(L) and j < len(R):
            if L[i].lastname < R[j].lastname:
                people[k] = L[i]
                i+=1
            else:
                people[k] = R[j]
                j+=1
            k+=1

        # Checks if any elements are left over
        while i < len(L):
            people[k] = L[i]
            i+=1
            k+=1

        while j < len(R):
            people[k] = R[j]
            j+=1
            k+=1

#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
#start = time.perf_counter()
#mergeSort(people)
#end = time.perf_counter()
#mergeSortTime = end - start
#print("Below here is the mergesort time")
#print(mergeSortTime)

# This function takes last element as pivot, places the pivot element at its correct position in sorted array, and
# places all smaller (smaller tha pivot) to left of pivot and all greater elements to right of pivot
def partition(people, low, high):
    i = ( low-1 ) # index of smaller element
    pivot = people[high].lastname # Pivot

    for j in range(low , high):
# if currerent element is smaller than or equal to pivot
        if people[j].lastname <= pivot:
# increment index of smaller element
            i = i+1
            people[i],people[j] = people[j],people[i]

    people[i+1],people[high] = people[high],people[i+1]
    return ( i+1 )

# The main function that implements quicksort
# arr[]--> array to be sorted, low --> starting index, high --> ending index
#this creates a defintion for the two variables low and high
low = 0
high = len(people)-1

#This is the function for how to do the Quick Sort
def quickSort(people,low,high):
    if low < high:
# pi is partition index, arr[P] is now at right place
        pi = partition(people,low,high)
# separately sort elements #before partion and after partion
        quickSort(people, low, pi-1)
        quickSort(people, pi+1, high)

#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
#start = time.perf_counter()
#quickSort(people, low, high)
#end = time.perf_counter()
#quickSortTime = end - start
#print("Below here is the quicksort time")
#print(quickSortTime)

#this function makes peoples last names to upper case that when the fucntion sorts it will not be affected
def pythonSort(people):
# some data had lowercase names which affects sorting
    people.sort(key=lambda person: person.lastname.upper(), reverse=False)

#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
start = time.perf_counter()
pythonSort(people)
end = time.perf_counter()
pythonSortTime = end - start
print("Below here is the pythonsort time")
print(pythonSortTime)

#This function is used to write .dat files to .csv files when a sorting algortihim is run
#outfile = open('mediumData.csv', 'w')

#for person in people:
#    outfile.write(str(person) + '\n')
#outfile.close()
