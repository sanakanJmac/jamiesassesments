#This imports the class or person.py file
from person import Person
import time

#this makes function read file data from csv files
fileref = open("largedata.csv","r")


lines = fileref.readlines()

#Defines the variables by making them equal to nothing
firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

#when the files are being read the file lines will split each liner with a ,
#it will then go ahead and define person equals all the elements defined above
for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(', ')
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

#this function will search csv file for a person named Pezzini, it does this by looping through and
#looking fore the last name pezzini,once pezzini is found itll print the looping elments and print pezzini details.
def linearSearch(people, searchString):
    for i in range (len(people)):
        if people[i].lastname == searchString:
            print(people[i].lastname)
            return i
    return -1

#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
#start = time.perf_counter()
#print(linearSearch (people, 'Pezzini'))
#end = time.perf_counter()
#linearSearchtime = end - start
#print(linearSearchtime)

# Data must be sorted by attribute being searched for if not found returns last item in list - this is a bug
# This function will define the first element as 0, this is the start of the person array.
def binarySearch(people, searchString):
    first = 0
    last = len(people)-1
    index = -1
    while (first <= last) and (index == -1):
        mid = (first+last)//2
        if people[mid].lastname == searchString:
            index = mid
        else:
            if searchString < people[mid].lastname:
                last = mid -1
            else:
                first = mid +1
    return print(people[index])

#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
#print(binarySearch(people, 'Pennyworth'))
#end = time.perf_counter()
#binarySearchtime = end - start
#print("Binary Search")
#print(binarySearchtime)

#This function searches the csv file for the person named Redmond, it does this by looping through the file looking for a match,
#Once a match is found itll print the result ie persons Remond details
def pythonSearch(people, searchString):
    for person in people:
        if searchString in person.lastname:
            return print(person)
    return i

#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
start = time.perf_counter()
print(pythonSearch(people, 'Redmond'))
end = time.perf_counter()
pythonSearchtime = end - start
print("Python Search")
print(pythonSearchtime)
