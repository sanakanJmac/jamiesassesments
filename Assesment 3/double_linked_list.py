#This imports the class or person.py file
from person import Person

import time

#this makes function read file data from csv files
fileref = open("smallData.csv","r")


lines = fileref.readlines()

#Defines the variables by making them equal to nothing
firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

#when the files are being read the file lines will split each liner with a ,
#it will then go ahead and define person equals all the elements defined above
for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(', ')
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

#A linked list node
class Node:

    # Consturctor for empty doubly linked list, this function when the program starts will create a new node where data is equal.
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

#class to create a doubly linked list
class DoublyLinkedList:

    #Constructor for empty doubly linked list
    def __init__(self):
        self.head = None

    #Given a reference to the head of a list an integer, inserts a new node on the front of list.
    def push(self, new_data):

        # 1. allocates node
        # 2. Puts the data in it
        new_node = Node(new_data)

        #3. Make next of new node as head and previous as none(alreadynone)
        new_node.next = self.head

        #4. change prev of head node to new_node
        if self.head is not None:
            self.head.prev = new_node

        #5. move the head to point to the new node
        self.head = new_node

    # Given a node as prev_node, insert a new node after the given node
    def insertAfter(self, prev_mode, new_data):

        #1. check if the given prev_node is none
        if prev_mode is None:
            print("this given previous node cannot be NULL")
            return

        #2. allocate new node
        #3. put in the new data
        new_node = Node(new_data)

        #4. make net of new node as next of prev node
        new_node.next = prev_mode.next

        #5. make prev_node as previous of new_node
        prev_mode.next = new_node

        #6. make prev_node ass prevous of new_node
        new_node.prev = prev_mode

        #7. change previous of new_node's next node, so if next new node is nothing itll revert to prev
        if new_node.next is not None:
            new_node.next.prev = new_node

    # Given a reference to the head of DLL and ineger, appends a new node at the end,
    # Attaches a new node
    def append(self, new_data):

        #1. Allocates node
        #2. Put in the data
        new_node = Node(new_data)

        #3. this new node is gong to be the last node, so make next of it as none
        new_node.next = None

        #4. if the liked list is empty, then make the new node as head
        if self.head is None:
            new_node.prev = None
            self.head = new_node
            return

        #5.else traverse till the last node
        last = self.head
        while last.next is not None:
            last = last.next

        # 6.Change the next of last node
        last.next = new_node

        #7.make last node as previous of new node
        new_node.prev = last

        return

    # This function prints contents of linked list, starting from the given node, it prints the traverseal forward in direction,
    # if the node is equal to nothing it'll print information
    def printList(self, node):
        print("\nTraversal in foward direction")
        while node is not None:
            print(node.data) #Print(" % d" % node.data,)
            last = node
            node = node.next

        #This prints the traversal in a reverse direction, itll print the doubly linked information in a reversed order
        print("\nTraversal in reverse direction")
        while last is not None:
            print(last.data,) #Print(" % d" % node.data,)
            last = last.prev

#This states that list one is euqal to doubly linked list
listOne = DoublyLinkedList()

for person in people:
    listOne.append(person)

#prints DLL for listone then start a timer for print time
print("Create DLL is: ",)
start = time.perf_counter()
listOne.printList(listOne.head)
end = time.perf_counter()
listOnetime = end - start
print("List one time:")
print(listOnetime)

#This states that list two is euqal to doubly linked list
listTwo = DoublyLinkedList()

#creates a loop for people and appends person at the start of listtwo
for person in people:
    listTwo.push(person)

#This is the time function, this starts a timer which counts the seconds it takes for the function to run.
print("Create DLL is: ",)
start = time.perf_counter()
listTwo.printList(listTwo.head)
end = time.perf_counter()
listTwotime = end - start
print("List two time:")
print(listTwotime)
