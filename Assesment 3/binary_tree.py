#This imports the class or person.py file
from person import Person

#imports the time liabrary
import time

#this makes function read file data from csv files
fileref = open("smallData.csv","r")

lines = fileref.readlines()

#Defines the variables by making them equal to nothing
firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

#when the files are being read the file lines will split each liner with a ,
#it will then go ahead and define person equals all the elements defined above
for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(', ')
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

#This creates a new class called node
class Node:

    #This creates a root, this will assign no properties left or right
    #it will also state that data is equal to itself
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data


    # Compare the new value with the parent node, it creates a left and right node.
    def insert(self, data):
        if self.data:
            if data.firstname < self.data.firstname:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)
            #firstname we add into the binary tree is higher than the first name from the csv file, it will make the
            #search start on the right side of the binary tree
            elif data.firstname > self.data.firstname:
                if self.right is None:
                    self.right = Node(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data


    # print the tree, prints specifically the left & right side of the binary tree also the csv file
    def PrintTree(self):
        if self.left:
            self.left.PrintTree()
        print(self.data),
        if self.right:
            self.right.PrintTree()

root = Node(people[0])
for person in people:
    root.insert(person)


#this will count how long the binary tree taks to run, will also insert the persons. it will print time taken output
start = time.perf_counter()
root.insert(Person('Harley', "Calvert", '10 Margate Avenue', 'FRANKSTON', 'VIC', '3199'))
root.insert(Person('Bob', 'Tranks', '18 Smith St', 'FRANKSTON', 'VIC', '3199'))
root.PrintTree()
end = time.perf_counter()
binarytreetime = end - start
print("Binary Tree Time:")
print(binarytreetime)
