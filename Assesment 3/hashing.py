#This imports the class or person.py file
from person import Person

#imports the time liabrary
import time

#this makes function read file data from csv files
fileref = open("smallData.csv","r")

lines = fileref.readlines()

#Defines the variables by making them equal to nothing
firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

#when the files are being read the file lines will split each liner with a ,
#it will then go ahead and define person equals all the elements defined above
for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(', ')
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

#this creates a set named people_dict, the algorithim will then loop throguh a person and then proceeds to hash all of the values
#in a person, will also count run time
start = time.perf_counter()
people_dict = {}
for person in people:
    people_dict[hash(person)] = person
    end = time.perf_counter()

#tprits out data in people_dict including the run time of the algorithim.
for k, v in people_dict.items():
    print(k, ':',v) #print key:value
hashingpeopletime = end - start
print("Hashing took:")
print(hashingpeopletime)
